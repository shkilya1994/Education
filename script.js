const buttonApp = document.getElementById('buttonApp')

const func1 = () => {
    buttonApp.style.background = 'red'
}

function func2() {
    buttonApp.style.background = 'black'
}

buttonApp.addEventListener('click', func2)